# Broker Logging utility module.

# imports
import sys, types
import logging
import os
import ntpath, posixpath
import re

# Class for logging p4broker filter scripts.
class blog:
   def __init__(self):
      # Constants
      self.p4home = os.getenv("P4HOME")
      self.file = os.path.join(self.p4home, "logs", "p4_get.log")

      # log global
      self.log = None

      # set up logging and read config data
      self.initLog()

   # Set up logging
   def initLog(self):
      self.log = logging.getLogger('blog')
      self.lfh = logging.FileHandler(self.file, mode='w', encoding=None, delay=False)
      self.formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
      self.lfh.setFormatter(self.formatter)
      self.log.addHandler(self.lfh)
      self.log.level=logging.DEBUG

   # Make logger available externally
   def getLogger(self):
      return self.log
