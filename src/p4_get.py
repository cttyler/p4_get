#!/Library/Frameworks/Python.framework/Versions/3.3/bin/python3
#--- !env python3
#------------------------------------------------------------------------------
# Copyright (c) 2014 Perforce Software, Inc.  Provided for use as defined in
# the Perforce Consulting Services Agreement.
#------------------------------------------------------------------------------

import os, errno
import sys, types
import logging
import blog
import re
from P4 import P4, P4Exception

# Globals to read from p4broker on stdin.
p4 = None
p4port = None
p4user = None
p4client = None
depotPath = None
ws = None
cmd = None
host = None
args = []
cwd = None
vals = dict()

# Other globals.
module = ""
revSpec = ""

def usage():
   print ("action: REJECT")
   print ("message: \"\n\nUsage:\n\tp4 get //path/to/module/...[@revSpec]\n\n\"")
   sys.exit()

def bad_usage (msg):
   print ("action: REJECT")
   print ("message: \"Bad Usage: %s\"" % msg)
   sys.exit()

# Parse broker arguments and store them in a dictionary for
# easy access.
def parseBrokerArgs():
   global depotPath
   data = sys.stdin.readlines()
   for arg in data:
      arg.rstrip('\n')
      m = re.match("^(.+?):\s+(.+)$", arg)
      k = m.group(1)
      v = m.group(2)
      vals[k] = v

   return vals

# Parse normal broker args
def parseArgs():
   global p4port
   global p4user
   global ws
   global cmd
   global host
   global args
   global cwd

   vals = parseBrokerArgs()

   argCount = vals['argCount']
   p4port = vals['brokerTargetPort']
   p4user = vals['user']
   cwd = vals['cwd']
   host = vals['clientHost']
   cmd = vals['command']
   ws = vals['workspace']
   for idx in range(int(argCount)):
      k = 'Arg' + str(idx)
      args.append(vals[k])
   log.debug("Read args %s" % ','.join(args))

   log.debug("All Args: %s" % args)

# Connect to Perforce
def initP4():
   global p4
   global p4client

   p4 = P4()
   #ticket_file = os.getenv ('P4TICKETS', 'UNDEFINED_P4TICKETS_VALUE')
   p4.prog = 'p4_get'
   p4.port = p4port
   p4.user = p4user
   p4.client = "p4get_ws"
   #p4.client = p4client
   #p4.ticket_file = ticket_file
   log.debug("Using P4USER=%s" % p4.user)
   log.debug("Using P4PORT=%s" % p4.port)
   #log.debug("Using P4TICKETS=%s" % p4.ticket_file)

   try:
      p4.connect()
      return True

   except P4Exception:
      log.error("Unable to connect to Perforce at P4PORT=%s" % p4port)
      for e in p4.errors:
         log.error("Errors: " + e)
      for w in p4.warnings:
         log.warn("Warnings: " + w)
      return False

#def mkdir_p(path):
#    try:
#        os.makedirs(path, "0o777", exist_ok=True)
#    except OSError as exc:
#        if exc.errno == errno.EEXIST and os.path.isdir(path):
#            pass
#        else: raise

# Main Program
blog = blog.blog()
log = blog.getLogger()
parseArgs()

if (len(args) == 0):
   usage()

syntaxOK = 0

for v in vals:
   log.debug ("Matching ^- in [%s]." % vals[v])
   if (re.match("^-", vals[v])):
      log.error ("Not handling command line flag [%s]." % v)

   log.debug ("Matching ^// in [%s]." % vals[v])
   if (re.match('^//', vals[v])):
      log.debug ("Searching /... in [%s]." % vals[v])
      if (re.search('/\.\.\.', vals[v])):
         if (re.search('@', vals[v])):
            depotPath = re.sub ('@.*$', '', vals[v])
            depotPath = re.sub ('^//', '', depotPath)
            revSpec = re.sub ('^.*@', '@', vals[v])
            module = re.sub ('/\.\.\..*$', '', depotPath)
            module = re.sub ('\/', '_', module)
            p4client = "%s_%s_%s" % (p4user, host, module)
            log.debug("DP=[%s] RS=[%s] M=[%s] [C=%s]" % (depotPath, revSpec, module, p4client))
            syntaxOK = 1
            break
         else:
            log.debug ("Searching /... in [%s]." % vals[v])
            if (re.search ('/\.\.\.$', vals[v])):
               depotPath = vals[v]
               depotPath = re.sub ('^//', '', depotPath)
               revSpec = ""
               module = re.sub ('/\.\.\..*$', '', depotPath)
               module = re.sub ('\/', '_', module)
               p4client = "%s_%s_%s" % (p4user, host, module)
               log.debug("DP=[%s] RS=[%s] M=[%s] [C=%s]" % (depotPath, revSpec, module, p4client))
               syntaxOK = 1
               break
            else:
               bad_usage ("Malformed depth path.  End with '/...' or '/...@revSpec'.")

if syntaxOK:
   log.debug ("Syntax verified.")
else:
   bad_usage ("Malformed depth path.  Start with '//', end with '/...[@revSpec]'.")

initP4()

cSpec = p4.fetch_client(p4client)
cSpec['Root'] = cwd
cSpec['Host'] = host
view = []
view.append("%s/... //%s/%s/..." % (depotPath, p4client, depotPath))
log.debug ("CSPEC: %s" % cSpec)

dir = "/p4/1/tmp/p4get_ws/user/%s/p4get" % p4user

try:
   os.makedirs(dir)
except OSError as exc:
   if exc.errno == errno.EEXIST and os.path.isdir(dir):
      pass
   else: raise

p4configFileName = "%s/.p4config.%s" % (dir, p4client)
p4configDepotFile = "//user/%s/p4get/.p4config.%s" % (p4user, p4client)
p4configFile = open (p4configFileName, 'wt')
p4configFile.write("P4PORT=p4:1666\nP4USER=%s\nP4CLIENT=%s\nP4IGNORE=.p4ignore\n" % (p4user, p4client))
p4configFile.close()

log.debug ("p4 reconcile %s" % p4configFileName)
try:
   p4.run_reconcile (p4configFileName)

except:
   pass

log.debug ("p4 submit -d \"%s\" %s" % (p4client, p4user))
try:
   p4.run_submit ("-d", "Added .p4config.%s for %s."  % (p4client, p4user), p4configFileName)
except:
   pass

log.debug ("p4 print -q -o .p4config.gen %s" % p4configDepotFile)
try:
   p4.run_print ("-q", "-o", ".p4config.gen", p4configDepotFile)
except:
   pass

csFileName = "/tmp/csFile.%s.tmp" % p4user
csFile = open (csFileName, 'wt')
csFile.write("Client: %s\n\nHost: %s\n\nRoot: %s\n\nView:\n\t//EDTIME_PATH/... //%s/...\n\n" % (p4client, host, cwd, p4client))
csFile.close()

print ("action: RESPOND")
print ("message: \"===== P4CONFIG FILE =====\n\n\nPut the following contents into your .p4config file:\nP4PORT=%s\nP4USER=%s\nP4CLIENT=%s\n\"" % (p4port, p4user, p4client))

